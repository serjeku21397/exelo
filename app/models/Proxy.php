<?php

namespace app\models;

class Proxy extends \yii\db\ActiveRecord
{
    const URL_IP = 'https://gist.githubusercontent.com/mrsombre/12c2b14225e096ba898d0e2e97e25510/raw/00783b601a6a67e662781e7351c8ddd1a0ade9cb/test.htm';

    static function tableName()
    {
        return 'proxy';
    }

    function beforeSave($insert)
    {
        if ($insert) {
            $this->timeCreated = time();
        }

        return parent::beforeSave($insert);
    }

    /**
     * Обновление ip
     *
     * @param array $ips - список ip в формате ip => port
     *
     * @return array
     */
    public static function updateIp($ips)
    {
        // Ip на удаление из БД
        $delete_ids = [];

        $query = static::find();

        foreach ($query->each() as $iProxy) {
            $ip = long2ip($iProxy->ip);
            $port = $iProxy->port;

            // Ищем ip(БД) среди новыйх ip
            if ( array_key_exists($ip, $ips) ){
                // Проверяем не изменился ли порт
                if ( $ips[$ip] != $port ){
                    $iProxy->port = $ips[$ip];
                    $iProxy->save();
                }

                // Если находим ip, значит он нам больше не понадобиться
                unset($ips[$ip]);
            }
            else{
                $delete_ids[] = $iProxy->id;
            }
        }

        return ['delete_ids' => $delete_ids, 'new_ips' => $ips];
    }

    /**
     * Добавление новых ip
     *
     * @param array $new_ips
     */
    public static function addIps($new_ips)
    {
        // Добавляем новые ip
        foreach ( $new_ips as $ip => $port ){
            $new_proxy = new static();
            $new_proxy->ip = ip2long($ip);
            $new_proxy->port = $port;
            $new_proxy->save();
        }
    }

    /**
     * Удаление ненужных ip
     *
     * @param array $delete_ids
     */
    public static function deleteIps($delete_ids)
    {
        // Удаляем ненужные ip
        if ( count($delete_ids) > 0 ){
            $delete_ids = implode(',', $delete_ids);
            static::deleteAll("id IN ({$delete_ids})");
        }
    }

    /**
     * Получить список ip за интервал времени по дате добавления
     *
     * @param int $date_from
     * @param int $date_to
     *
     * @return array
     */
    public static function getListIp($date_from, $date_to)
    {
        return static::find()
            ->select(['inet_ntoa(ip)'])
            ->where(['between', 'timeCreated', $date_from, $date_to])
            ->asArray()
            ->column();
    }
}
