<?php

namespace app\models;

use GuzzleHttp\Exception\ConnectException;
use Yii;
use yii\base\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Class Proxy
 * @package app\models
 * Класс для парсинга и обработки ip
 *
 * public $url - url по которому производим парсинг ip
 * public $parse_ips - хранилище обработанных ip в формате ip => port
 * public $file - путь к файлу в который будем сохранять данные
 */
class WorkerIP extends Model
{
    public $url;
    public $parse_ips = [];
    public $file;


    public function init()
    {
        $this->url = 'https://gist.githubusercontent.com/mrsombre/12c2b14225e096ba898d0e2e97e25510/raw/00783b601a6a67e662781e7351c8ddd1a0ade9cb/test.htm';
        $this->file = Yii::$app->basePath.'/../data/ip.save';
        parent::init();
    }

    /**
     * Парсинг DOM`а страницы с последующим сохранение в файл
     *
     * @return bool
     */
    public function parseIp()
    {
        try{
            // Настраиваем клиент guzzle
            $client = new Client([
                'base_uri' => $this->url
            ]);
            // Отправляем запрос и записываем содиржимое в файл
            $client->request('GET', '', ['sink' => $this->file]);
            return true;
        }
        catch (ConnectException $e) {
            return false;
        }
    }

    /**
     * Получение ip из HTML файла
     *
     * @return array $parse_ips
     */
    public function getIpFromDOM()
    {
        $dom = new \DOMDocument();
        $dom->loadHTMLFile($this->file);
        // Получаем все элементы td в виде массива
        $elements = $dom->getElementsByTagName('td');

        foreach ( $elements as $key => $el ){
            // Проверяем является ли элемент ip
            if ( filter_var($el->nodeValue, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ){
                $this->parse_ips[$el->nodeValue] = $elements[$key + 1]->nodeValue;
            }
        }
    }
}