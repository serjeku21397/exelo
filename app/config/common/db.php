<?php

use yii\helpers\ArrayHelper;

$config = [
    'host' => 'localhost',
    'dbname' => 'extest',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8'
];

// локальные настройки
if (file_exists(__DIR__ . '/db.local.php')) {
    $config = ArrayHelper::merge($config, require(__DIR__ . '/db.local.php'));
}

return $config;
