<?php

return [
    'components' => [
        'log' => [
            'targets' => [
                'firebug_db' => [
                    'class' => app\yiix\debug\FirebugDbLogTarget::class,
                    'enabled' => true,
                    'groupName' => 'DbLog'
                ]
            ]
        ]
    ],
];
