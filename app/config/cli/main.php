<?php

use yii\helpers\ArrayHelper;

$parent = require(__DIR__ . '/../common/yii.php');
$config = [
    'id' => 'app.cli',
    'language' => 'ru-RU',
    'enableCoreCommands' => false,
    'controllerNamespace' => 'app\\controllers',
    'bootstrap' => [
        'log' => 'log'
    ],
    'components' => [
        'log' => [
            'targets' => [
                'error' => [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error']
                ]
            ]
        ]
    ]
];
$config = ArrayHelper::merge($parent, $config);

// локальные настройки
if (file_exists(__DIR__ . '/main.local.php')) {
    $config = ArrayHelper::merge($config, require(__DIR__ . '/main.local.php'));
}

return $config;
