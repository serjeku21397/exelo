<?php

namespace app\controllers;

use app\models\Proxy as arProxy;
use app\models\WorkerIP;
use Yii;

use yii\helpers\Json;

/**
 * Class TestController
 * @package app\controllers
 */
class IpController extends \yii\console\Controller
{
    /**
     * Обновление ip
     *
     * @param string $url
     */
    public function actionUpdate($url = '')
    {
        // Запускаем класс для работы с ip
        $worker_ip = new WorkerIP();

        // Если передан новый url
        if ( !empty($url) ){
            $worker_ip->url = $url;
        }

        // Получаем информацию с ресурса
        if ( !$worker_ip->parseIp() ){
            throw new \InvalidArgumentException("Request URL unavailable");
        }

        // Получаем ip
        $worker_ip->getIpFromDOM();

        // Обновляем существующие ip и получаем ip на добавления и id на удаление
        $result = arProxy::updateIp($worker_ip->parse_ips);

        // Добавляем новые ip
        arProxy::addIps($result['new_ips']);

        // Удаляем ненужны ip
        arProxy::deleteIps($result['delete_ids']);
    }

    /**
     * Список актуальных ip
     *
     * @param string $date_from
     * @param string $date_to
     */
    public function actionSelect($date_from, $date_to){
        $is_number = (filter_var($date_from, FILTER_VALIDATE_INT) && filter_var($date_to, FILTER_VALIDATE_INT));

        // Проверяем даты
        if ( (!strtotime($date_from) || !strtotime($date_to)) && !$is_number ){
            throw new \InvalidArgumentException("Wrong param type.");
        }
        elseif( !$is_number ){
            $date_from = strtotime($date_from);
            $date_to = strtotime($date_to);
        }

        // Получаем список ip и приводи к формату json
        $data_proxy = Json::encode(arProxy::getListIp($date_from, $date_to));

        $this->stdout("{$data_proxy}\n");
    }
}
