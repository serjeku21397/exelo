
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `proxy`
--

DROP TABLE IF EXISTS `proxy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proxy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` int(10) unsigned NOT NULL,
  `port` smallint(5) unsigned NOT NULL,
  `timeCreated` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proxy`
--

LOCK TABLES `proxy` WRITE;
/*!40000 ALTER TABLE `proxy` DISABLE KEYS */;
INSERT INTO `proxy` VALUES (1,1759514727,3128,1499406632),(2,2319167746,8080,1479406632),(3,3305472248,8080,1479406632),(4,1264827348,9999,1479406632),(5,916764657,80,1499406632),(6,918238367,80,1479406632),(7,2307368673,8130,1479406632),(8,3496218386,3128,1499406632),(9,2382397660,8088,1479406632),(10,3239762948,3128,1479406632),(11,1747309826,80,1479406632),(12,632978895,8080,1479406632),(13,2822706195,8145,1479406632),(14,2822706195,8122,1479406632),(15,3232344743,8080,1479406632),(16,2307368673,8138,1479406632),(17,3232344743,80,1479406632),(18,3289078329,8080,1479406632),(19,2584772566,8080,1479406632),(20,1796552745,80,1479406632),(21,760005381,8080,1479406632),(22,2307368673,8131,1479406632),(23,3319449129,8080,1479406632),(24,597429106,8080,1479406632),(25,1208881409,8080,1479406632),(26,2307368673,8141,1479406632),(27,910383313,80,1479406632),(28,391671915,80,1479406632),(29,3232344671,8080,1479406632),(30,3232345232,8080,1479406632),(31,1599505734,8080,1479406632),(32,3323503090,8080,1479406632),(33,3648486758,8081,1479406632),(34,3495547852,80,1479406632),(35,3560228449,8080,1479406632),(36,2732546291,80,1479406632),(37,702474810,8080,1479406632),(38,2503484386,80,1479406632),(39,921421184,1337,1500006632),(40,3232345232,80,1479406632),(41,915781527,3128,1499406632),(42,3281159588,8080,1499406632),(43,2328168452,80,1479406632),(44,1578374438,3128,1479406632),(45,2733889397,8080,1479406632),(46,691749690,8080,1479406632),(47,3249232386,8080,1479406632),(48,693058762,8080,1479406632),(49,1268173645,8118,1479406632),(50,3496196407,80,1479406632);
/*!40000 ALTER TABLE `proxy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
